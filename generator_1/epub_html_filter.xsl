<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2022  Stephan Kreutzer

This file is part of generator_1 of Hyper-Augmentation Magazine.

generator_1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

generator_1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with generator_1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" xmlns:person="htx-scheme-id://org.schema.20050114T185202Z/Person" exclude-result-prefixes="xhtml">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="@*[name()!='xml:space']|node()">
    <xsl:copy>
      <!-- For XPath 2.0: "@* except xml:space|node()" -->
      <xsl:apply-templates select="@*[name()!='xml:space']|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="//xhtml:script"/>
  <xsl:template match="//xhtml:style"/>

  <xsl:template match="//person:Person">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="//person:Person//node()">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="//person:Person//text()">
    <xsl:value-of select="."/>
  </xsl:template>

</xsl:stylesheet>
