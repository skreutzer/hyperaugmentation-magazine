<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2020-2023 Stephan Kreutzer

This file is part of generator_1 of Hyper-Augmentation Magazine.

generator_1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

generator_1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with generator_1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ncx="http://www.daisy.org/z3986/2005/ncx/" exclude-result-prefixes="ncx">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">
    <resource-retriever-1-workflow-jobfile>
      <xsl:comment> This file was created by ncx_to_resource_retriever_1_jobfile.xsl of generator_1 for Hyper-Augmentation Magazine, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/hyperaugmentation-magazine/ and https://publishing-systems.org). </xsl:comment>
      <resources>
        <xsl:apply-templates select=".//ncx:content"/>
      </resources>
      <output-directory path="../htx/"/>
    </resource-retriever-1-workflow-jobfile>
  </xsl:template>

  <xsl:template match="//ncx:content">
    <!-- Currently not needed, as references are absolute.
    <resource identifier="http://hyper-augmentation.org/{@src}"/ -->
    <resource identifier="{@src}"/>
  </xsl:template>

  <xsl:template match="node()|@*|text()"/>

</xsl:stylesheet>
