#!/bin/sh
# Copyright (C) 2020-2021 Stephan Kreutzer
#
# This file is part of generator_1 of Hyper-Augmentation Magazine.
#
# generator_1 is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# generator_1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with generator_1. If not, see <http://www.gnu.org/licenses/>.

mkdir -p ./htx/
mkdir -p ./temp/

java -cp ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/ resource_retriever_1 ./jobfile_resource_retriever_1.xml ./temp/resultinfo_resource_retriever_1.xml

mv ./htx/resource_0 ./htx/index.ncx

java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_ncx_to_resource_retriever_1_jobfile.xml ./temp/resultinfo_xml_xslt_transformator_1_ncx_to_resource_retriever_1_jobfile.xml
java -cp ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/ resource_retriever_1 ./temp/jobfile_resource_retriever_1.xml ./temp/resultinfo_resource_retriever_1.xml
