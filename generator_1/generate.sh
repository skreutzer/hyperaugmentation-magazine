#!/bin/sh
# Copyright (C) 2020-2022 Stephan Kreutzer
#
# This file is part of generator_1 of Hyper-Augmentation Magazine.
#
# generator_1 is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# generator_1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with generator_1. If not, see <http://www.gnu.org/licenses/>.

mkdir -p ./temp/
mkdir -p ./html/
mkdir -p ./epub/temp/
mkdir -p ./latex_1/


printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./temp/jobfile_xml_xslt_transformator_1_html_filter.xml
printf "<xml-xslt-transformator-1-jobfile>\n" >> ./temp/jobfile_xml_xslt_transformator_1_html_filter.xml

for filepath in ./htx/resource_*
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    printf "  <job input-file=\"../htx/%s\" entities-resolver-config-file=\"../digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/entities/config_xhtml_1_1.xml\" stylesheet-file=\"../epub_html_filter.xsl\" output-file=\"../html/%s.xhtml\" input-file-equals-output-file-overwrite=\"false\"/>\n" $file $file >> ./temp/jobfile_xml_xslt_transformator_1_html_filter.xml
done

printf "</xml-xslt-transformator-1-jobfile>\n" >> ./temp/jobfile_xml_xslt_transformator_1_html_filter.xml

java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./temp/jobfile_xml_xslt_transformator_1_html_filter.xml ./temp/resultinfo_xml_xslt_transformator_1_html_filter.xml


java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 jobfile_xml_xslt_transformator_1_ncx_to_html2epub2_jobfile.xml ./temp/resultinfo_xml_xslt_transformator_1_ncx_to_html2epub2_jobfile.xml

java -cp ./automated_digital_publishing/html2epub/html2epub1/workflows/ html2epub1_config_merge1 ./temp/jobfile_html2epub2.xml ./metadata.xml ./temp/jobfile_html2epub2_merged.xml
java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./temp/jobfile_html2epub2_merged.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/hyper-augmentation.epub
rm -rf ./epub/temp/*


for filepath in ./htx/resource_*
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./temp/jobfile_xhtml_to_latex_1.xml
    printf "<xhtml-to-latex-1-workflow-jobfile>\n" >> ./temp/jobfile_xhtml_to_latex_1.xml
    printf "  <!-- This file was created by generate.sh of generator_1 for Hyper-Augmentation Magazine, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/hyperaugmentation-magazine/ and https://publishing-systems.org). -->\n" >> ./temp/jobfile_xhtml_to_latex_1.xml
    printf "  <input-file path=\"../htx/%s\"/>\n" $file >> ./temp/jobfile_xhtml_to_latex_1.xml
    printf "  <stylesheet-file path=\"../digital_publishing_workflow_tools/latex/layouts/xhtml_to_latex_1.xsl\"/>\n" >> ./temp/jobfile_xhtml_to_latex_1.xml
    printf "  <output-file path=\"../latex_1/%s.tmp.tex\"/>\n" $filename >> ./temp/jobfile_xhtml_to_latex_1.xml
    printf "</xhtml-to-latex-1-workflow-jobfile>\n" >> ./temp/jobfile_xhtml_to_latex_1.xml

    java -cp ./digital_publishing_workflow_tools/workflows/xhtml_to_latex/xhtml_to_latex_1/ xhtml_to_latex_1 ./temp/jobfile_xhtml_to_latex_1.xml ./temp/resultinfo_xhtml_to_latex_1.xml

    rm ./temp/jobfile_xhtml_to_latex_1.xml

    java -cp ./automated_digital_publishing/txtreplace/txtreplace1/ txtreplace1 ./latex_1/$filename.tmp.tex ./txtreplace1_replacement_dictionary_latex.xml ./latex_1/$filename.tex
    rm ./latex_1/$filename.tmp.tex

    cd ./latex_1/

    xelatex $filename.tex
    xelatex $filename.tex
    xelatex $filename.tex
    xelatex $filename.tex

    cd ..
done
